package main

import (
	"fmt"
)

type matrix struct {
	a [][]int
}

type sparseMatrixJennings struct {
	an []int
	d  []int
}

type SparseMatrixRainbaltdMestenyi struct {
	an []int
	nr []int
	nc []int
	jr []int
	jc []int
}

func PackRM(m matrix) SparseMatrixRainbaltdMestenyi {
	lengthY := len(m.a)
	lengthX := len(m.a[0])

	var an = make([]int, 0)[:]
	var nr = make([]int, 0)[:]
	var nc = make([]int, 0)[:]
	var jr = make([]int, lengthY)
	for i := range jr {
		jr[i] = -1
	}
	var jc = make([]int, lengthX)
	for i := range jc {
		jc[i] = -1
	}

	for i := 0; i < lengthY; i++ {
		for j := 0; j < lengthX; j++ {
			if m.a[i][j] != 0 {
				an = append(an, m.a[i][j])
				idx := len(an) - 1
				nr = append(nr, -1)
				nc = append(nc, -1)
				// check should we set values to jr, jc
				if jr[i] == -1 {
					jr[i] = idx
				} else {
					checkIdx := jr[i]
					done := false
					for !done {
						if nr[checkIdx] == -1 {
							nr[checkIdx] = idx
							done = true
						} else {
							checkIdx = nr[checkIdx]
						}
					}
				}
				if jc[j] == -1 {
					jc[j] = idx
				} else {
					checkIdx := jc[j]
					done := false
					for !done {
						if nc[checkIdx] == -1 {
							nc[checkIdx] = idx
							done = true
						} else {
							checkIdx = nc[checkIdx]
						}
					}
				}
			}
		}
		// cycle row 
		checkIdx := jr[i]
		if checkIdx != -1 {
			done := false
			for !done {
				if nr[checkIdx] == -1 {
					nr[checkIdx] = jr[i]
					done = true
				} else {
					checkIdx = nr[checkIdx]
				}
			}
		}

	}
	// cycle column 
	for j:=0; j<lengthX; j++ {
		checkIdx := jc[j]
		if checkIdx != -1 {
			done := false
			for !done {
				if nc[checkIdx] == -1 {
					nc[checkIdx] = jc[j]
					done = true
				} else {
					checkIdx = nc[checkIdx]
				}
			}
		}
	}
	return SparseMatrixRainbaltdMestenyi{an, nr, nc, jr, jc}
}

func UnpackRM(sm SparseMatrixRainbaltdMestenyi) matrix {
	m := NewMatrix(len(sm.jr), len(sm.jc))
	for idx, val := range sm.an {
		// надо найти координаты этого элемента
		i := findIdxInSlice(sm.jr, idx)
		if (i == -1) {
			// надо найти первого в столбце и запомнить его координату в jc
			done := false
			currIdx := idx
			for !done {
				nextIdx := sm.nr[currIdx]
				if nextIdx < currIdx {
					done = true
					i = findIdxInSlice(sm.jr, nextIdx)
				} else {
					currIdx = nextIdx
				}
			}
		}
		j := findIdxInSlice(sm.jc, idx)
		if (j == -1) {
			done := false
			currIdx := idx
			for !done {
				nextIdx := sm.nc[currIdx]
				if nextIdx < currIdx {
					done = true
					j = findIdxInSlice(sm.jc, nextIdx)
				} else {
					currIdx = nextIdx
				}
			}
		}
		m.a[i][j] = val
	}
	return m
}

func NewEmptySparseMatrix(lengthX, lengthY int) SparseMatrixRainbaltdMestenyi {
	var jr = make([]int, lengthY)
	for i := range jr {
		jr[i] = -1
	}
	var jc = make([]int, lengthX)
	for i := range jc {
		jc[i] = -1
	}
	return SparseMatrixRainbaltdMestenyi{
		make([]int, 0)[:], 
		make([]int, 0)[:], 
		make([]int, 0)[:], 
		jr, 
		jc,
	}
}

func MultiplyRM(a, b SparseMatrixRainbaltdMestenyi) SparseMatrixRainbaltdMestenyi {
	if len(a.jc) != len(b.jr) {
		panic("Cannot multiply by definition")
	}
	lengthY := len(a.jr)
	lengthX := len(b.jc)
	smResult := NewEmptySparseMatrix(lengthX, lengthY)

	for i:=0; i<lengthY; i++ {
		for j:=0; j<lengthX; j++ {
			sum := 0
			q := a.jr[i]
			w := b.jc[j]
			for q != -1 && w != -1 {
				_, qColumn := findCoordinates(q, a)
				wRow, _ := findCoordinates(w, b)
				if (qColumn == wRow) {
					sum += a.an[q] * b.an[w]
					q = a.nr[q]
					w = b.nc[w]
				} else if qColumn > wRow {
					w = b.nc[w]
				} else if wRow > qColumn {
					q = a.nr[q]
				}
				if q == a.jr[i] {
					q = -1
				}
				if w == b.jc[i] {
					w = -1
				}
			}
			if sum != 0 {
				appendElementRM(&smResult, i, j, sum)
			}
		}
	}

	return smResult
}

func AddRM(a, b SparseMatrixRainbaltdMestenyi) SparseMatrixRainbaltdMestenyi {
	lengthY := len(a.jr)
	lengthX := len(a.jc)
	smResult := NewEmptySparseMatrix(lengthX, lengthY)

	done := false
	// create cusors both for a.an and b.an
	q := 0
	w := 0

	for !done {
		ai, aj := findCoordinates(q, a)
		bi, bj := findCoordinates(w, b)
	
		if (ai < bi) {
			appendElementRM(&smResult, ai, aj, a.an[q])
			q++
		} else if (bi < ai) {
			appendElementRM(&smResult, bi, bj, b.an[w])
			w++
		} else {
			if (aj < bj) {
				appendElementRM(&smResult, ai, aj, a.an[q])
				q++
			} else if (bj < aj) {
				appendElementRM(&smResult, bi, bj, b.an[w])
				w++
			} else {
				// ai == bi && aj == bj
				sum := a.an[q] + b.an[w]
				if sum != 0 {
					appendElementRM(&smResult, ai, aj, sum)
				}
				q++
				w++
			}
		}
		if q >= len(a.an) {
			for ; w<len(b.an); w++ {
				ci, cj := findCoordinates(w, b)
				appendElementRM(&smResult, ci, cj, b.an[w])
			}
			done = true;
		}
		if w >= len(b.an) {
			for ; q<len(a.an); q++ {
				ci, cj := findCoordinates(q, a)
				appendElementRM(&smResult, ci, cj, a.an[q])
			}
			done = true;
		}
	}

	return smResult;
}

func appendElementRM(a *SparseMatrixRainbaltdMestenyi, i int, j int, value int) {
	a.an = append(a.an, value)
	idx := len(a.an) - 1
	if a.jr[i] == -1 {
		a.jr[i] = idx
	}
	a.nr = append(a.nr, a.jr[i])
	if a.jr[i] != idx {
		for currIdx := a.jr[i];; {
			nextIdx := a.nr[currIdx]
			if nextIdx == a.jr[i] && nextIdx != idx {
				a.nr[currIdx] = idx
				break
			}
			currIdx = nextIdx
		}
	}
	
	if a.jc[j] == -1 {
		a.jc[j] = idx
	}
	a.nc = append(a.nc, a.jc[j])
	if a.jc[j] != idx {
		for currIdx := a.jc[j];; {
			nextIdx := a.nc[currIdx]
			if nextIdx == a.jc[j] && nextIdx != idx {
				a.nc[currIdx] = idx
				break
			}
			currIdx = nextIdx
		}
	}

}

func findFirstInRing(idx int, arr []int) int {
	done := false
	currIdx := idx
	ringEnterIdx := currIdx
	for !done {
		nextIdx := arr[currIdx]
		if nextIdx <= currIdx {
			ringEnterIdx = nextIdx;
			done = true
		}
		currIdx = nextIdx
	}
	return ringEnterIdx;
}

func findPostionInArray(elem int, arr []int) int {
	result := -1
	for i:=0; i<len(arr); i++ {
		if arr[i] == elem {
			result = i;
			break;
		}
	}
	return result
}

func findCoordinates(idx int, a SparseMatrixRainbaltdMestenyi) (int, int) {
	// need to find i as row index
	rowEnterIdx := findFirstInRing(idx, a.nr)
	iCoord := findPostionInArray(rowEnterIdx, a.jr)
	
	columnEnterIdx := findFirstInRing(idx, a.nc)
	jCoord := findPostionInArray(columnEnterIdx, a.jc)

	return iCoord, jCoord
}

func findIdxInSlice(sl []int, value int) int {
	for i:=0; i<len(sl); i++ {
		if sl[i] == value {
			return i
		}
	}
	return -1
}


func SparseAdd(sa, sb sparseMatrixJennings) sparseMatrixJennings {
	length := len(sa.d)
	if len(sa.d) != len(sb.d) {
		panic("Different size of sparse matrix")
	}
	d := make([]int, 0)[:]
	an := make([]int, 0)[:]

	an = append(an, sa.an[0]+sb.an[0]) // r[0][0]
	d = append(d, 0)

	for i := 1; i < length; i++ {
		saRowSlice := sa.an[sa.d[i-1]+1 : sa.d[i]+1]
		sbRowSlice := sb.an[sb.d[i-1]+1 : sb.d[i]+1]
		resultSlice := addSlices(saRowSlice, sbRowSlice)
		an = append(an, resultSlice...)
		d = append(d, len(an)-1)
	}
	return sparseMatrixJennings{an, d}
}

func addSlices(s1, s2 []int) []int {
	revS1 := reverseSlice(s1)
	revS2 := reverseSlice(s2)
	revRes := make([]int, 0)
	var smallerRevS []int
	var biggerRevS []int
	if len(s1) < len(s2) {
		smallerRevS = revS1
		biggerRevS = revS2
	} else {
		smallerRevS = revS2
		biggerRevS = revS1
	}
	for i := 0; i < len(smallerRevS); i++ {
		revRes = append(revRes, smallerRevS[i]+biggerRevS[i])
	}
	for j := len(smallerRevS); j < len(biggerRevS); j++ {
		revRes = append(revRes, biggerRevS[j])
	}
	// remove zeroes at end
	done := false
	for !done {
		if revRes[len(revRes)-1] == 0 {
			revRes = revRes[:len(revRes)-1]
		} else {
			done = true
		}
	}

	return reverseSlice(revRes)
}

func Unpack(sm sparseMatrixJennings) matrix {
	length := len(sm.d)
	ma := make([][]int, length)
	ma[0] = make([]int, length)
	ma[0][0] = sm.an[0]

	for i := 1; i < length; i++ {
		ma[i] = make([]int, length)
		s := i
		for j := sm.d[i]; j > sm.d[i-1]; j-- {
			ma[i][s] = sm.an[j]
			ma[s][i] = sm.an[j]
			s -= 1
		}
	}

	return matrix{ma}
}

func reverseSlice(b []int) []int {
	a := make([]int, len(b))
	copy(a, b)
	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}
	return a
}

func Pack(m matrix) sparseMatrixJennings {
	a := m.a
	lentgh := len(m.a)
	d := make([]int, 0)
	var san = make([]int, 0)[:]
	for i := 0; i < lentgh; i++ {
		addZeroes := false
		for j := 0; j <= i; j++ {
			if i == j {
				san = append(san, a[i][j])
				d = append(d, len(san)-1)
				break
			}
			if a[i][j] != 0 {
				addZeroes = true
				san = append(san, a[i][j])
			} else {
				if addZeroes {
					san = append(san, 0)
				}
			}
		}
	}
	return sparseMatrixJennings{san, d}
}

func NewMatrix(y, x int) matrix {
	a := make([][]int, y)
	for i := range a {
		a[i] = make([]int, x)
	}
	return matrix{a}
}

func (m *matrix) rows() int {
	return len(m.a)
}

func (m *matrix) columns() int {
	return len(m.a[0])
}

func Add(a, b matrix) matrix {
	if a.columns() != b.columns() || a.rows() != b.rows() {
		panic("Not same shape matrix")
	}
	r := NewMatrix(a.rows(), a.columns())
	for i := 0; i < a.rows(); i++ {
		for j := 0; j < a.columns(); j++ {
			r.a[i][j] = a.a[i][j] + b.a[i][j]
		}
	}
	return r
}

func (m matrix) String() string {
	var buf string
	for _, elem := range m.a {
		buf += fmt.Sprintf("%v\n", elem)
	}
	return string(buf)
}

func main() {
	fmt.Println("hi")
}

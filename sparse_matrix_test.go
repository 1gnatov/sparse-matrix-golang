package main

import (
	"reflect"
	"testing"
)

func TestPack(t *testing.T) {
	m := matrix{[][]int{
		{10, 0, 0, 0, 0},
		{0, 20, 2, 7, 0},
		{0, 2, 30, 0, 0},
		{0, 7, 0, 40, 9},
		{0, 0, 0, 9, 50},
	}}

	sm := Pack(m)
	if !reflect.DeepEqual(sm.an, []int{10, 20, 2, 30, 7, 0, 40, 9, 50}) {
		panic("")
	}
	if !reflect.DeepEqual(sm.d, []int{0, 1, 3, 6, 8}) {
		panic("")
	}
}

func TestUnpack(t *testing.T) {
	sm := sparseMatrixJennings{[]int{10, 20, 2, 30, 7, 0, 40, 9, 50}, []int{0, 1, 3, 6, 8}}
	m := Unpack(sm)
	expectMatrix := matrix{[][]int{
		{10, 0, 0, 0, 0},
		{0, 20, 2, 7, 0},
		{0, 2, 30, 0, 0},
		{0, 7, 0, 40, 9},
		{0, 0, 0, 9, 50},
	}}
	if !reflect.DeepEqual(m, expectMatrix) {
		panic("")
	}
}

func TestAddPacked(t *testing.T) {
	a := matrix{[][]int{
		{10, 0, 0, 0, 0},
		{0, 20, 2, 7, 0},
		{0, 2, 30, 0, 0},
		{0, 7, 0, 40, 9},
		{0, 0, 0, 9, 50},
	}}
	b := matrix{[][]int{
		{1, 3, 0, 0, 0},
		{3, 4, 0, -7, 0},
		{0, 0, -30, 0, 10},
		{0, -7, 0, 8, 1},
		{0, 0, 10, 1, 0},
	}}
	sum := Add(a, b)
	sa := Pack(a)
	sb := Pack(b)
	ssum := SparseAdd(sa, sb)
	if !reflect.DeepEqual(sparseMatrixJennings{
		[]int{11, 3, 24, 2, 0, 48, 10, 10, 50},
		[]int{0, 2, 4, 5, 8},
	}, ssum) {
		panic("Not correct sparseMatrix after sum")
	}

	if !reflect.DeepEqual(Pack(sum), ssum) {
		panic("Not equal pack sum")
	}

	if !reflect.DeepEqual(sum, Unpack(ssum)) {
		panic("Not equal unpack sum")
	}
}

func TestAdd(t *testing.T) {
	a := matrix{[][]int{
		{1, 1, 1},
		{2, 2, 2},
	}}
	b := matrix{[][]int{
		{0, 1, 2},
		{3, 4, 5},
	}}
	expect := matrix{[][]int{
		{1, 2, 3},
		{5, 6, 7},
	}}
	res := Add(a, b)
	if !reflect.DeepEqual(res, expect) {
		panic("")
	}
}

func TestPackRM(t *testing.T) {
	m := matrix{[][]int{
		{0, 0, 10, 0, 0, 50, 0},
		{20, 0, 90, 40, 0, 0, 80},
		{0, 0, 0, 0, 0, 0, 0},
		{60, 0, 30, 0, 0, 0, 70},
	}}
	sm := PackRM(m)
	expected := SparseMatrixRainbaltdMestenyi{
		[]int{10, 50, 20, 90, 40, 80, 60, 30, 70},
		[]int{1, 0, 3, 4, 5, 2, 7, 8, 6},
		[]int{3, 1, 6, 7, 4, 8, 2, 0, 5},
		[]int{0, 2, -1, 6},
		[]int{2, -1, 0, 4, -1, 1, 5},
	}
	if !reflect.DeepEqual(sm, expected) {
		panic("")
	}
}

func TestUnpackRM(t *testing.T) {
	sm := SparseMatrixRainbaltdMestenyi{
		[]int{10, 50, 20, 90, 40, 80, 60, 30, 70},
		[]int{1, 0, 3, 4, 5, 2, 7, 8, 6},
		[]int{3, 1, 6, 7, 4, 8, 2, 0, 5},
		[]int{0, 2, -1, 6},
		[]int{2, -1, 0, 4, -1, 1, 5},
	}
	expect := matrix{[][]int{
		{0, 0, 10, 0, 0, 50, 0},
		{20, 0, 90, 40, 0, 0, 80},
		{0, 0, 0, 0, 0, 0, 0},
		{60, 0, 30, 0, 0, 0, 70},
	}}
	if !reflect.DeepEqual(UnpackRM(sm), expect) {
		panic("")
	}
}

func TestAddRM(t *testing.T) {
	asm := PackRM(matrix{[][]int{
		{0, 10, 0, 50, 0},
		{20, 0, 40, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 30, 0, 60, 70},
	}})
	bsm := PackRM(matrix{[][]int{
		{0, 0, 100, 200, 300},
		{-20, 0, -40, 0, 0},
		{0, 0, 0, 500, 0},
		{0, -30, 200, 0, 0},
	}})

	sm := AddRM(asm, bsm)
	expectMatrix := matrix{[][]int{
		{0, 10, 100, 250, 300},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 500, 0},
		{0, 0, 200, 60, 70},
	}}
	expSm := SparseMatrixRainbaltdMestenyi{
		//     0    1    2    3    4    5   6   7
		[]int{10, 100, 250, 300, 500, 200, 60, 70}, //an
		[]int{1, 2, 3, 0, 4, 6, 7, 5},              //nr
		[]int{0, 5, 4, 7, 6, 1, 2, 3},              //nc
		[]int{0, -1, 4, 5},                         //jr
		[]int{-1, 0, 1, 2, 3},                      //jc
	}

	if !reflect.DeepEqual(expSm, PackRM(expectMatrix)) {
		panic("Invalid expSm eq expectM")
	}
	if !reflect.DeepEqual(sm, expSm) {
		panic("Invalid sum")
	}
}

//https://www.wolframalpha.com/input/?i=%7B%7B1%2C+2%2C+3%7D%2C+%7B0%2C+0%2C+0%7D%2C%7B4%2C+5%2C+6%7D%2C%7B0%2C+0%2C+0%7D%7D+*+%7B%7B10%2C+0%2C+30%2C+0%7D%2C+%09%09%7B40%2C+0%2C+60%2C+0%7D%2C+%09%09%7B70%2C+0%2C+80%2C+0%7D%7D
func TestMultiplyRM(t *testing.T) {
	asm := PackRM(matrix{[][]int{
		{1, 2, 3},
		{0, 0, 0},
		{4, 5, 6},
		{0, 0, 0},
	}})
	bsm := PackRM(matrix{[][]int{
		{10, 0, 30, 0},
		{40, 0, 60, 0},
		{70, 0, 80, 0},
	}})
	expect := matrix{[][]int{
		{300, 0, 390, 0},
		{0, 0, 0, 0},
		{660, 0, 900, 0},
		{0, 0, 0, 0},
	}}
	expectSm := SparseMatrixRainbaltdMestenyi{
		[]int{300, 390, 660, 900},
		[]int{1, 0, 3, 2},
		[]int{2, 3, 0, 1},
		[]int{0, -1, 2, -1},
		[]int{0, -1, 1, -1},
	}
	if !reflect.DeepEqual(PackRM(expect), expectSm) {
		panic("Expectactions are not equal")
	}
	resm := MultiplyRM(asm, bsm)
	if !reflect.DeepEqual(resm, expectSm) {
		panic("Invalid multiplication")
	}
	if !reflect.DeepEqual(UnpackRM(resm), expect) {
		panic("Invalid unpack")
	}
}
